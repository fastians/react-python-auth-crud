import React, { useState, useEffect } from "react";
import { RequireToken } from "./Auth";
import { Routes, Route } from "react-router-dom";
import Login from "./Login";
import Profile from "./Profile";

const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/login" element={<Login />} />
        <Route
          path="/profile"
          element={
            <RequireToken>
              <Profile />
            </RequireToken>
          }
        />
      </Routes>
    </div>
  );
};

export default App;
