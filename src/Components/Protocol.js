import React, { useState } from "react";
import Step from "./Step";
export default function Protocol() {
  const [steps, setSteps] = useState([]);
  function addStep(){
    var newStep = [...steps];
    newStep.push(1);
    setSteps(newStep);
  }
  function removeItem(id){
    var newStep = [...steps];
    newStep.push(1);
    setSteps(newStep);
  }
  return (
    <div>
      Protocol
      {steps.map((item) => {
        return <Step />;
      })}
      <button onClick={() => addStep}> Add Step</button>
    </div>
  );
}
