import { useNavigate } from "react-router";
import { useState } from "react";
import data from './sample.json';
export default function Profile() {
  const navigate = useNavigate();

  const signOut = () => {
    localStorage.removeItem("userID");
    navigate("/");
  };
  const [files, setFiles] = useState("");
  const handleChange = (e) => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = (e) => {
      console.log("e.target.result", e.target.result);
      
      validateFile(e.target.result);
    };
  };
  const validateFile = (data) => {
    try {
      var myObject = JSON.parse(data);
      setFiles(myObject);
      // console.log(myObject.posts);
    } catch (e) {
      return false;
    }
    return myObject;
  };
  return (
    <>
      <div style={{ marginTop: 20, minHeight: 700 }}>
        <h1>Profile page</h1>
        <p>Hello there, welcome to your profile page</p>
        <div>
          <input type="file" onChange={handleChange} accept=".json"/>
          
        </div>
        {files && files.metadata.protocolName} 
        {data.metadata.protocolName}

        
        <button onClick={signOut}>sign out</button>
      </div>
    </>
  );
}